package mission.secret;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecretMissionController {
    @GetMapping("/api/status")
    public String index() {
        return "Mission completed.";
    }
}
