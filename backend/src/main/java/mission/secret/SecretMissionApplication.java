package mission.secret;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecretMissionApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecretMissionApplication.class, args);
    }
}
